package com.miraclesoft.io.repository;

import org.springframework.data.repository.CrudRepository;

import com.miraclesoft.io.model.HealthCare;

public interface HealthCareRepository extends CrudRepository<HealthCare, Long>{

}
