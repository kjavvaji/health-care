package com.miraclesoft.io.repository;

import org.springframework.data.repository.CrudRepository;

import com.miraclesoft.io.model.MedicalConditions;

public interface MedicalConditionsRepository extends CrudRepository<MedicalConditions, Long> {
	
	
    
}
