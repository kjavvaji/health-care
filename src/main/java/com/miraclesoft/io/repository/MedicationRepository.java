package com.miraclesoft.io.repository;

import org.springframework.data.repository.CrudRepository;

import com.miraclesoft.io.model.Medication_Details;

public interface MedicationRepository extends CrudRepository<Medication_Details,Long>{

}
