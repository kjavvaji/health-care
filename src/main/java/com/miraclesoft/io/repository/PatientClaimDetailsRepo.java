package com.miraclesoft.io.repository;

import org.springframework.data.jpa.repository.JpaRepository;

import com.miraclesoft.io.model.PatientClaimDetails;

public interface PatientClaimDetailsRepo extends JpaRepository<PatientClaimDetails, Integer>{

}
