package com.miraclesoft.io.repository;

import org.springframework.data.jpa.repository.JpaRepository;

import com.miraclesoft.io.model.PatientInsurancePlan;

public interface PatientInsurancePlanRepo extends JpaRepository<PatientInsurancePlan, Integer>{

}
