FROM openjdk:8
EXPOSE 8085
ADD target/Workshop-0.0.1-SNAPSHOT.jar Workshop-0.0.1-SNAPSHOT.jar
ENTRYPOINT ["java", "-jar", "Workshop-0.0.1-SNAPSHOT.jar"]